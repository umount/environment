# http://alias.sh/identify-and-search-active-network-connections
spy () {
    lsof -i -P +c 0 +M | grep -i "$1"
}

alias tma='tmux -2 attach -d || tmux -2 new'

# Git
source /etc/bash_completion.d/git-prompt
export PS1='[\u@\h:\w $(__git_ps1 "(%s)")]\$ '

__define_git_completion () {
eval "
    _git_$2_shortcut () {
        COMP_LINE=\"git $2\${COMP_LINE#$1}\"
        let COMP_POINT+=$((4+${#2}-${#1}))
        COMP_WORDS=(git $2 \"\${COMP_WORDS[@]:1}\")
        let COMP_CWORD+=1

        local cur words cword prev
        _get_comp_words_by_ref -n =: cur words cword prev
        _git_$2
    }
"
}

__git_shortcut () {
    type _git_$2_shortcut &>/dev/null || __define_git_completion $1 $2
    alias $1="git $2 $3"
    complete -o default -o nospace -F _git_$2_shortcut $1
}

__git_shortcut  ga    add
__git_shortcut  gb    branch
__git_shortcut  gba   branch -a
__git_shortcut  gc    commit '-v -m'
__git_shortcut  gca   commit '-a -v -m'
__git_shortcut  gco   checkout
__git_shortcut  gd    diff
__git_shortcut  gdc   diff --cached
__git_shortcut  gds   diff --stat
__git_shortcut  gf    fetch
__git_shortcut  gl    log
__git_shortcut  glp   log -p
__git_shortcut  gls   log --stat
__git_shortcut  gs    status
__git_shortcut  gull  pull
__git_shortcut  gsub  submodule
__git_shortcut  gush  push

# http://alias.sh/extract-most-know-archives-one-command
extract () {
    if [ -f $1 ] ; then
      case $1 in
        *.tar.bz2)   tar xjf $1     ;;
        *.tar.gz)    tar xzf $1     ;;
        *.bz2)       bunzip2 $1     ;;
        *.rar)       unrar e $1     ;;
        *.gz)        gunzip $1      ;;
        *.tar)       tar xf $1      ;;
        *.tbz2)      tar xjf $1     ;;
        *.tgz)       tar xzf $1     ;;
        *.zip)       unzip $1       ;;
        *.Z)         uncompress $1  ;;
        *.7z)        7z x $1        ;;
        *)     echo "'$1' cannot be extracted via extract()" ;;
         esac
     else
         echo "'$1' is not a valid file"
     fi
}
