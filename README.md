## Instalation
 -------------
Install [Vundle](https://github.com/gmarik/Vundle.vim):

`git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/vundle`

Install from command line: `vim +PluginInstall +qall`
 -------------
Create [Tmuxline](https://github.com/edkolev/tmuxline.vim) configuration file
 -------------
Download and install [Powerline font](https://github.com/Lokaltog/powerline-fonts)
 -------------
